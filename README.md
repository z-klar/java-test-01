**REST Service (Spring Boot app) for communication with Papouch's Quido I/O module**

It implements the interfaces according to the description:

[RTR API / Papouch QUIDO](https://confluence.dqa.cz/dqi/display/E4IN/RTR%3A+API)

With following exceptions:

| API | Impelementation Status |
| ------ | ------ |
| outputs | Fully implemented |
| inputs | Fully implemented |
| temperature |NOT Implemented (no Temp. sensor available) |
| status  | NOT Implemented (for interesting values just a combination of inputs and outputs  |

Configuration - mandatory:
*  application.properties: quido.url (quido default = http://192.168.1.254)
 
Configuration - optional:
*  application.properties: server.port=xxxx (if not used, the built-in Tomcat uses port 8080)
  
For reading any type of value (inputs, outputs) the method {quido.url}/fresh.xml is used. This 
call returns a XML file describing current status of all module items. The requested
item(s) is parsed and converted to the JSON form for response.

For setting outputs the the method {quido.url}/set.xml&type=X&id=Y, where X is either 'r' for 
setting to 0 or 's' for setting to 1. Y is the ID of required output (with our module 1 to 16)


