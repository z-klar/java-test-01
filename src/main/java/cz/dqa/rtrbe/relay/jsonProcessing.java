package cz.dqa.rtrbe.relay;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.*;

import java.util.ArrayList;
import java.util.Iterator;

public class jsonProcessing {

    /**********************************************************************
     Important note: the method parser.parse() checks the overall
     syntax of the JSON string and if there is any error (anywhere)
     it throws en exception. Therefore it is not possible to get
     exact info e.g. which key has wrong value
    ***********************************************************************/
    public static String getSingleJsonElement(String src, String key) {

        Object obj = null, obj2;
        String sResult = "";
        JSONObject jsobject;

        try {
            JSONParser parser=new JSONParser();
            obj = parser.parse(src);
            jsobject = (JSONObject) obj;
            obj2 = jsobject.get(key);
            if(obj2 == null) sResult = String.format("ERR: Key [%s] NOT present", key);
            else sResult = obj2.toString();
            return(sResult);
        }
        catch(Exception ex) {
            sResult = String.format("ERR Key [%s]:", key);
            sResult += ex.getMessage();

            StackTraceElement [] trace;
            int i;
            sResult += "Exception:";
            sResult += ex.getClass().toString();
            sResult += ex.getMessage();
            sResult += "Stack Trace:";
            trace = ex.getStackTrace();
            i = 0;
            for (StackTraceElement se: trace) {
                if(se.getClassName().startsWith("cz"))
                    sResult += String.format("Class:%s Method: %s Line: %d",
                        se.getClassName(), se.getMethodName(), se.getLineNumber());
                i++;
                if(i>4)  break;
            }
            return(sResult);
        }
    }

    /******************************************************************************
     * Create JSONArray with the data related to all OUTPUTS in the QUIDO's
     * XML status file (fresh.xml)
     * @param xmlDoc input XML document
     * @param jsa Output JSONArray: data from the XML file are converted into it
     * @return List of errors or empty list in case of OK
     *****************************************************************************/
    public static ArrayList<String> GetOutputStates(Document xmlDoc, JSONArray jsa) {

        ArrayList<String> al = new ArrayList<>();

        try {
            Element root = xmlDoc.getDocumentElement();
            NodeList nodelist = root.getChildNodes();
            String sid, sname, status, svalue, smode, nodename;
            NamedNodeMap attr;
            //JSONArray js_array = new JSONArray();

            for (int i = 0; i < nodelist.getLength(); i++) {
                nodename = nodelist.item(i).getNodeName();
                if (nodename.compareTo("dout") == 0) {
                    JSONObject obj = new JSONObject();
                    attr = nodelist.item(i).getAttributes();
                    sid = FindAttrValue(attr, "id");
                    sname = FindAttrValue(attr, "name");
                    status = FindAttrValue(attr, "sts");
                    svalue = FindAttrValue(attr, "val");
                    smode = FindAttrValue(attr, "mde");

                    obj.put("id", Integer.parseInt(sid));
                    obj.put("name", sname);
                    obj.put("status", Integer.parseInt(status));
                    obj.put("value", svalue.contains("0") ? false : true);
                    obj.put("mode", Integer.parseInt(smode));
                    obj.put("time", 0);
                    jsa.add(obj);
                }
            }
            return (al);
        }
        catch(Exception ex) {
            LogException(ex, al);
            return(al);
        }
    }

    /******************************************************************************
     * Create JSONArray with the data related to all INPUTS in the QUIDO's
     * XML status file (fresh.xml)
     * @param xmlDoc input XML document
     * @param jsa Output JSONArray: data from the XML file are converted into it
     * @return List of errors or empty list in case of OK
     *****************************************************************************/
    public static ArrayList<String> GetInputStates(Document xmlDoc, JSONArray jsa) {

        ArrayList<String> al = new ArrayList<>();

        try {
            Element root = xmlDoc.getDocumentElement();
            NodeList nodelist = root.getChildNodes();
            String sid, sname, status, svalue, smode, nodename, scounter;
            NamedNodeMap attr;
            //JSONArray js_array = new JSONArray();

            for (int i = 0; i < nodelist.getLength(); i++) {
                nodename = nodelist.item(i).getNodeName();
                if (nodename.compareTo("din") == 0) {
                    JSONObject obj = new JSONObject();
                    attr = nodelist.item(i).getAttributes();
                    sid = FindAttrValue(attr, "id");
                    sname = FindAttrValue(attr, "name");
                    status = FindAttrValue(attr, "sts");
                    svalue = FindAttrValue(attr, "val");
                    smode = FindAttrValue(attr, "cmo");
                    scounter = FindAttrValue(attr, "cnt");

                    obj.put("id", Integer.parseInt(sid));
                    obj.put("name", sname);
                    obj.put("counter", Integer.parseInt(scounter));
                    obj.put("counterMode", Integer.parseInt(smode));
                    obj.put("status", Integer.parseInt(status));
                    obj.put("value", svalue.contains("0") ? false : true);
                    jsa.add(obj);
                }
            }
            return (al);
        }
        catch(Exception ex) {
            LogException(ex, al);
            return(al);
        }
    }


    /**********************************************************************************
     * Create a JSONObject with data related to the required OUTPUT (nid param)
     * @param xmlDoc input XML document
     * @param jso output JSONObject with the retrieved data
     * @param nid required output ID
     * @return text array with errors or empty array
     **********************************************************************************/
    public static ArrayList<String> GetSingleOutputState(Document xmlDoc, JSONObject jso, int nid) {

        ArrayList<String> al = new ArrayList<>();
        int pocet = 0;

        try {
            Element root = xmlDoc.getDocumentElement();
            NodeList nodelist = root.getChildNodes();
            String sid, sname, status, svalue, smode, nodename;
            NamedNodeMap attr;
            //JSONArray js_array = new JSONArray();

            for (int i = 0; i < nodelist.getLength(); i++) {
                nodename = nodelist.item(i).getNodeName();
                if (nodename.compareTo("dout") == 0) {
                    attr = nodelist.item(i).getAttributes();
                    sid = FindAttrValue(attr, "id");
                    if(Integer.parseInt(sid) == nid) {
                        pocet++;
                        sname = FindAttrValue(attr, "name");
                        status = FindAttrValue(attr, "sts");
                        svalue = FindAttrValue(attr, "val");
                        smode = FindAttrValue(attr, "mde");

                        jso.put("id", Integer.parseInt(sid));
                        jso.put("name", sname);
                        jso.put("status", Integer.parseInt(status));
                        jso.put("value", svalue.contains("0") ? false : true);
                        jso.put("mode", Integer.parseInt(smode));
                        jso.put("time", 0);
                    }
                }
            }
            if(pocet == 0) al.add("Requested Relay ID NOT Found !!");
            return (al);
        }
        catch(Exception ex) {
            LogException(ex, al);
            return(al);
        }
    }

    /**********************************************************************************
     * Create a JSONObject with data related to the required INPUT (nid param)
     * @param xmlDoc input XML document
     * @param jso output JSONObject with the retrieved data
     * @param nid required output ID
     * @return text array with errors or empty array
     **********************************************************************************/
    public static ArrayList<String> GetSingleInputState(Document xmlDoc, JSONObject jso, int nid) {

        ArrayList<String> al = new ArrayList<>();
        int pocet = 0;

        try {
            Element root = xmlDoc.getDocumentElement();
            NodeList nodelist = root.getChildNodes();
            String sid, sname, status, svalue, smode, nodename, scounter;
            NamedNodeMap attr;
            //JSONArray js_array = new JSONArray();

            for (int i = 0; i < nodelist.getLength(); i++) {
                nodename = nodelist.item(i).getNodeName();
                if (nodename.compareTo("din") == 0) {
                    attr = nodelist.item(i).getAttributes();
                    sid = FindAttrValue(attr, "id");
                    if(Integer.parseInt(sid) == nid) {
                        pocet++;
                        attr = nodelist.item(i).getAttributes();
                        sid = FindAttrValue(attr, "id");
                        sname = FindAttrValue(attr, "name");
                        status = FindAttrValue(attr, "sts");
                        svalue = FindAttrValue(attr, "val");
                        smode = FindAttrValue(attr, "cmo");
                        scounter = FindAttrValue(attr, "cnt");

                        jso.put("id", Integer.parseInt(sid));
                        jso.put("name", sname);
                        jso.put("counter", Integer.parseInt(scounter));
                        jso.put("counterMode", Integer.parseInt(smode));
                        jso.put("status", Integer.parseInt(status));
                        jso.put("value", svalue.contains("0") ? false : true);
                    }
                }
            }
            if(pocet == 0) al.add("Requested Relay ID NOT Found !!");
            return (al);
        }
        catch(Exception ex) {
            LogException(ex, al);
            return(al);
        }
    }



    /******************************************************************************
     * Test method for verification of XML parsing
     * @param xmlDoc input XML document
     * @return parsed data in text form
     *****************************************************************************/
    public static ArrayList<String> GetOutputStatesText(Document xmlDoc, JSONObject outObject) {
        ArrayList<String > al = new ArrayList<>();
        NamedNodeMap attr;
        String sid, sname, status, svalue, smode;

        Element root = xmlDoc.getDocumentElement();
        NodeList nodelist = root.getChildNodes();

        for (int i = 0; i <nodelist.getLength() ; i++) {
            String spom2 = String.format("Name=%s ", nodelist.item(i).getNodeName());
            if(nodelist.item(i).getNodeName().compareTo("dout") == 0) {
                attr = nodelist.item(i).getAttributes();
                sid = FindAttrValue(attr, "id");
                sname = FindAttrValue(attr, "name");
                status = FindAttrValue(attr, "sts");
                svalue = FindAttrValue(attr, "val");
                smode = FindAttrValue(attr, "mde");

                spom2 += String.format("ID=%s  Name=%s  status=%s  value=%s  mode=%s",
                                            sid, sname, status, svalue, smode);

                al.add(spom2);
            }
        }
        return(al);
    }

    /******************************************************************************
     *  Method takes the input JSON object (JSON array with required items
     *  from the PATCH /rtr-relay/outputs) to the array of setRequest
     *  structures, which can be easily processed
     * @param strJson string with the input JSON array
     * @param alData output array of setRequest items
     * @return text array with error messages or empty array in case of OK
     *****************************************************************************/
    public static  ArrayList<String> GetRequestList(String strJson, ArrayList<setRequest> alData) {
        ArrayList<String> al = new ArrayList<>();
        JSONArray jsarray;
        Object obj = null, obj1;
        JSONObject obh2;
        String classname, spom;

        try {
            JSONParser parser=new JSONParser();
            obj = parser.parse(strJson);
            jsarray = (JSONArray) obj;

            Iterator iter = jsarray.iterator();
            while(iter.hasNext()) {
                obj1 = iter.next();
                classname = obj1.getClass().getName();
                if(classname.contains("JSONObject")) {
                    obh2 = (JSONObject) obj1;
                    setRequest sr = new setRequest(0, true, 0);
                    spom = getSingleJsonElementFromObject(obh2, "id");
                    try { sr.mId = Integer.parseInt(spom); }
                    catch(Exception ex) {sr.mErr = spom; }
                    spom = getSingleJsonElementFromObject(obh2, "value");
                    if(spom.startsWith("tru")) sr.mValue = true;
                    else if(spom.startsWith("fal")) sr.mValue = false;
                    else sr.mErr = spom;
                    alData.add(sr);
                }
                else {
                    al.add(String.format("GetRequestList: Unknown object type - %s", classname));
                    return(al);
                }
            }
            return(al);
        }
        catch(Exception ex) {
            LogException(ex, al);
            return (al);
        }
    }


    /**************************************************************************************
     * Gets the element with the given KEY from the provided JSON object
     * @param obk input JSON object
     * @param key requested key
     * @return string representation of the object (if found) or error message
     *         starting with 'ERR'
     *************************************************************************************/
    private static String getSingleJsonElementFromObject(JSONObject obk, String key) {

        Object obj = null, obj2;
        String sResult = "";

        try {
            obj2 = obk.get(key);
            if(obj2 == null) sResult = String.format("ERR: Key [%s] NOT present", key);
            else sResult = obj2.toString();
            return(sResult);
        }
        catch(Exception ex) {
            sResult = String.format("ERR Key [%s]:", key);
            sResult += ex.getMessage();

            StackTraceElement [] trace;
            int i;
            sResult += String.format("Exception:");
            sResult += ex.getClass().toString();
            sResult += ex.getMessage();
            sResult += "Stack Trace:";
            trace = ex.getStackTrace();
            i = 0;
            for (StackTraceElement se: trace) {
                if(se.getClassName().startsWith("cz"))
                    sResult += String.format("Class:%s Method: %s Line: %d",
                        se.getClassName(), se.getMethodName(), se.getLineNumber());
                i++;
                if(i>400)  break;
            }
            return(sResult);
        }
    }




    /******************************************************************************
     * Find required attribute from the attribute array (attribute array of
     * a XML node)
     * @param attr list of all node's attributes
     * @param attrname required name of the desired attribute
     * @return the value of the attribute (if found) or 'N/A' string
     *****************************************************************************/
    private static String FindAttrValue(NamedNodeMap attr, String attrname) {
        String sres = "N/A";
        int poc = attr.getLength();
        for(int j = 0; j< poc; j++) {
            Attr attrib = (Attr) attr.item(j);
            String attrName = attrib.getNodeName();
            String attrValue = attrib.getNodeValue();
            if (attrName.compareTo(attrname) == 0) {
                sres = attrValue;
                break;
            }
        } return(sres);
    }

    /******************************************************************************
     * Log an exception: store some more detailed information about the
     * provided exception into provided ArrayList
     * @param ex exception thrown
     * @param al text array for adding the exception info
     *****************************************************************************/
    private static void LogException(Exception ex, ArrayList al) {

        al.add(ex.getMessage());
        StackTraceElement [] trace;
        int i;
        al.add(ex.getClass().toString());
        al.add("Stack Trace:");
        trace = ex.getStackTrace();
        i = 0;
        for (StackTraceElement se: trace) {
            if(se.getClassName().startsWith("cz"))
                al.add(String.format("Class:%s Method: %s Line: %d",
                        se.getClassName(), se.getMethodName(), se.getLineNumber()));
            i++;
            if(i>400)  break;
        }
    }


}
