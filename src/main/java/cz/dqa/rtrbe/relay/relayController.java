package cz.dqa.rtrbe.relay;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.w3c.dom.Document;


/*******************************************************************************
 * Test of IDEA / GITLAB integration
 * Try # 2
 * Try # 3
 * Try # 4
 *******************************************************************************/

@RestController
public class relayController {

    private final String mappingRoot = "/rtr-relay";
    private final String VERSION = "1.0.0.0";
    private final Logger log = LoggerFactory.getLogger(relayController.class);

    @Value("${quido.url}")
    private  String quidoUrl;

    /****************************************************************************
     **************************  OUTPUTS   **************************************
     ***************************************************************************/
    // GET  /rtr-relay/outputs
    @RequestMapping(value = mappingRoot + "/outputs")
    public ResponseEntity<Object> getOutputs() {

        Document xmlDDoc;
        JSONArray jsa = new JSONArray();
        ArrayList<String> al;

        xmlDDoc = GetStatus();
        if(xmlDDoc == null) {
            return new ResponseEntity<>("Error retrieving FRESH.XML", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            al = jsonProcessing.GetOutputStates(xmlDDoc, jsa);
            if(al.size() == 0) {
                return new ResponseEntity<>(jsa, HttpStatus.OK);
            }
            else {
                String spom = "";
                for (String s : al) { spom += s;  spom += "\r\n";  }
                return new ResponseEntity<>("GET outputs: internal error: " + spom, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /***************************************************************************************
    // PATCH  /rtr-relay/outputs
    **************************************************************************************/
    @RequestMapping(value = mappingRoot + "/outputs", method = RequestMethod.PATCH)
    public ResponseEntity<Object> patchUotput(@RequestBody String sJson) {

        int relayId, iRes, iVal;
        ArrayList<String> alResult;
        ArrayList<setRequest> alData = new ArrayList<>();
        String sErr = "";

        log.debug("PATCH /outputs/");

        alResult = jsonProcessing.GetRequestList(sJson, alData);
        if(alResult.size() > 0) {
            return new ResponseEntity<>("Wrong JSON content format!", HttpStatus.GONE);
        }

        for (setRequest alDatum : alData) {
            relayId = alDatum.mId;
            iVal = (alDatum.mValue ? 1 : 0);
            if ((relayId > 0) && (relayId < 17)) {
                iRes = UpdateRelay(relayId, iVal);
                if (iRes != 0) {
                    sErr += String.format("Error updating relay[%d]   !", relayId);
                }
            } else {
                sErr += String.format("Wrong relayID: [%d]   !", relayId);
            }
        }

        if(sErr.length() == 0) {
            return new ResponseEntity<>("Relays are updated successsfully.", HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>("Errors when patching relays: " + sErr, HttpStatus.GONE);
        }
    }

    /***************************************************************************************
    // GET  /rtr-relay/outputs/{id}
    ***************************************************************************************/
    @RequestMapping(value = mappingRoot + "/outputs/{id}")
    public ResponseEntity<Object> getSingleOutput(@PathVariable String id) {
        int nid;

        try { nid = Integer.parseInt(id); }
        catch(Exception ex) {
            return new ResponseEntity<>("GET rtr-relay/outputs: WRONG ID "+ id, HttpStatus.BAD_REQUEST);
        }

        Document xmlDDoc;
        JSONObject jso = new JSONObject();
        ArrayList<String> al;

        xmlDDoc = GetStatus();
        if(xmlDDoc == null) {
            return new ResponseEntity<>("Error retrieving FRESH.XML", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            al = jsonProcessing.GetSingleOutputState(xmlDDoc, jso, nid);
            if(al.size() == 0) {
                return new ResponseEntity<>(jso, HttpStatus.OK);
            }
            else {
                String spom = "";
                for (String s : al) { spom += s; spom += "\r\n"; }
                return new ResponseEntity<>("GET outputs: internal error: " + spom, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

    }

    /***************************************************************************************
    // PUT  /rtr-relay/outputs/{id}
    ***************************************************************************************/
    @RequestMapping(value = mappingRoot + "/outputs/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> setSingleOutput(@PathVariable("id") String id, @RequestBody String sJson) {

        String outid, value="", sRes;
        boolean bErr = false;
        int relayId, iRes, iVal;

        log.debug(String.format("PUT /outputs/%s", id));

        outid = jsonProcessing.getSingleJsonElement(sJson, "id");
        if(outid.startsWith("ERR")) {
            bErr = true;
            log.error(String.format("%s", outid));
        }
        else {
            value = jsonProcessing.getSingleJsonElement(sJson, "value");
            if (value.startsWith("ERR")) {
                bErr = true;
                log.error(String.format("%s", outid));
            }
        }
        if(bErr) {
            return new ResponseEntity<>("Wrong JSON content format!", HttpStatus.GONE);
        }
        //--------------------------------------------------------------------------
        try {
            relayId = Integer.parseInt(outid);
        }
        catch(Exception ex) {
            return new ResponseEntity<>("Wrong ID format", HttpStatus.GONE);
        }
        if((relayId<1) || (relayId>16)) {
            return new ResponseEntity<>("Invalid relay ID", HttpStatus.GONE);
        }
        iVal = -1;
        if(value.startsWith("tru")) iVal = 1;
        else if(value.startsWith("fal")) iVal = 0;
        if(iVal == -1) {
            String spom = String.format("Wrong required value: [%s]", value);
            return new ResponseEntity<>(spom, HttpStatus.GONE);
        }
        //--------------------------------------------------------------------------
        iRes = UpdateRelay(relayId, iVal);
        if(iRes == 0) {
            sRes = String.format("ID=%s   VALUE=%s  ", outid, value);
            return new ResponseEntity<>("Relay is updated successsfully with:  " + sRes, HttpStatus.OK);
        }
        else {
            sRes = String.format("Error sending message to QUIDO: %d", iRes);
            return new ResponseEntity<>(sRes, HttpStatus.BAD_REQUEST);
        }
    }

    /****************************************************************************
     ********************************  INPUTS   *********************************
     ***************************************************************************/
    // GET  /rtr-relay/inputs
    @RequestMapping(value = mappingRoot + "/inputs")
    public ResponseEntity<Object> getInputs() {

        Document xmlDDoc;
        JSONArray jsa = new JSONArray();
        ArrayList<String> al;

        xmlDDoc = GetStatus();
        if(xmlDDoc == null) {
            return new ResponseEntity<>("Error retrieving FRESH.XML", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            al = jsonProcessing.GetInputStates(xmlDDoc, jsa);
            if(al.size() == 0) {
                return new ResponseEntity<>(jsa, HttpStatus.OK);
            }
            else {
                String spom = "";
                for (String s : al) { spom += s;  spom += "\r\n";  }
                return new ResponseEntity<>("GET inputs: internal error: " + spom, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    //------------------------------------------------------
    // GET  /rtr-relay/inputs/{id}
    @RequestMapping(value = mappingRoot + "/inputs/{id}")
    public ResponseEntity<Object> getSingleInput(@PathVariable String id) {
        int nid;

        try { nid = Integer.parseInt(id); }
        catch(Exception ex) {
            return new ResponseEntity<>("GET rtr-relay/inputs: WRONG ID "+ id, HttpStatus.BAD_REQUEST);
        }

        Document xmlDDoc;
        JSONObject jso = new JSONObject();
        ArrayList<String> al;

        xmlDDoc = GetStatus();
        if(xmlDDoc == null) {
            return new ResponseEntity<>("Error retrieving FRESH.XML", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        else {
            al = jsonProcessing.GetSingleInputState(xmlDDoc, jso, nid);
            if(al.size() == 0) {
                return new ResponseEntity<>(jso, HttpStatus.OK);
            }
            else {
                String spom = "";
                for (String s : al) { spom += s; spom += "\r\n"; }
                return new ResponseEntity<>("GET inputs: internal error: " + spom, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }

    /****************************************************************************
     **************************  TEMPERATURE   **********************************
     ***************************************************************************/
    // GET  /rtr-relay/temperature
    @RequestMapping(value = mappingRoot + "/temperature")
    public ResponseEntity<Object> getTemperature() {

        return new ResponseEntity<>("GET rtr-relay/temperature", HttpStatus.NOT_IMPLEMENTED);
    }

    /****************************************************************************
     *******************************  STATUS   **********************************
     ***************************************************************************/
    // GET  /rtr-relay/status
    @RequestMapping(value = mappingRoot + "/status")
    public ResponseEntity<Object> getStatus() {

        return new ResponseEntity<>("GET rtr-relay/status", HttpStatus.NOT_IMPLEMENTED);
    }


    /****************************************************************************
     *******************************  VERSION   *********************************
     ***************************************************************************/
    // GET  /rtr-relay/version
    @RequestMapping(value = mappingRoot + "/version")
    public ResponseEntity<Object> getVersion() {

        JSONObject jso = new JSONObject();
        jso.put("verion",  VERSION);
        return new ResponseEntity<>(jso, HttpStatus.OK);
    }


    /****************************************************************************
     ******************  R e l a y   c o m m u n i c a t i o n   ****************
     ***************************************************************************/
    private int UpdateRelay(int relayId, int value) {
        String spom, spar;

        spom = String.format("Command: Relay[%d] = %d", relayId, value);
        log.info(spom);

        if(value == 0) spar = "r";
        else spar = "s";
        spom = String.format("%s/set.xml?type=%s&id=%d",
                        quidoUrl,  spar,      relayId);
        log.info(spom);

        try {
            URL url = new URL(spom);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(5000);
            con.setRequestMethod("GET");
            int status = con.getResponseCode();
            con.disconnect();
            return(0);
        }
        catch (Exception ex) {
            LogException(ex);
            return(-1);
        }
    }

    /*-------------------------------------------------------------------
      Get the XML file fresh.xml from Quido and parse it into a
      XML document
    --------------------------------------------------------------------*/
    private Document GetStatus() {

        Document document;
        String xmlString, spom;

        //spom = "http://192.168.1.254/fresh.xml";
        spom = String.format("%s/fresh.xml", quidoUrl);
        log.info(spom);

        try {
            URL url = new URL(spom);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(5000);
            con.setRequestMethod("GET");
            int status = con.getResponseCode();
            log.info(String.format("Response code: %d", status));
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder content = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            xmlString = content.toString();
            in.close();
            con.disconnect();

            //log.info(xmlString);
            document = xmlProcessing.ConvertStringToXmlDoc(xmlString);
            return(document);
        }
        catch (Exception ex) {
            LogException(ex);
            return(null);
        }
    }

    /*-------------------------------------------------------------------

    --------------------------------------------------------------------*/
    private void LogException(Exception ex) {

        log.error("Exception: " + ex.getMessage());
        StackTraceElement [] trace;
        int i;
        log.error(ex.getClass().toString());
        log.error("Stack Trace:");
        trace = ex.getStackTrace();
        i = 0;
        for (StackTraceElement se: trace) {
            if(se.getClassName().startsWith("cz"))
                log.error(String.format("Class:%s Method: %s Line: %d",
                        se.getClassName(), se.getMethodName(), se.getLineNumber()));
            i++;
            if(i>400)  break;
        }
    }

}
