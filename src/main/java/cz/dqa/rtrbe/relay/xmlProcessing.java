package cz.dqa.rtrbe.relay;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class xmlProcessing {

    /***************************************************************************
     *
     * @param srcXml string containing the input XML file
     * @return XML document - result of parsing the input file
     ***************************************************************************/
    public static Document ConvertStringToXmlDoc(String srcXml) {

        //Parser that produces DOM object trees from XML content
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        //API to obtain DOM Document instance
        DocumentBuilder builder;
        try
        {
            //Create DocumentBuilder with default configuration
            builder = factory.newDocumentBuilder();

            //Parse the content to Document object
            StringReader stringreader = new StringReader(srcXml);
            Document doc = builder.parse(new InputSource(stringreader));
            return doc;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }


}
