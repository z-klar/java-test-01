package cz.dqa.rtrbe.relay;

/******************************************************************************
 * Tool class for passing data values
 *****************************************************************************/
public class setRequest {

    public int mId;
    public boolean mValue;
    public int mTime;
    public String mErr;

    public setRequest(int id, boolean val, int t) {
        mId = id;
        mValue = val;
        mTime = t;
        mErr = "";
    }
}
