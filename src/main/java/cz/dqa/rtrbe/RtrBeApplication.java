package cz.dqa.rtrbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RtrBeApplication {

	public static void main(String[] args) {

		SpringApplication.run(RtrBeApplication.class, args);
	}

}
